<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
//$routes->get('/', 'Home::index');
$routes->get('/', 'ReportController::index');

$routes->match(['get','post'],'content', 'Home::listContent');
$routes->match(['get','post'],'profile', 'Home::profile');
$routes->get('content/(:segment)','Home::detailContent/$1');
$routes->get('content/download/(:segment)', 'Home::downloadFile/$1');
$routes->get('content/delete/(:segment)', 'Home::deleteContent/$1');

$routes->match(['get','post'],'content/edit/(:segment)', 'Home::updateContent/$1');
$routes->post('content/add', 'Home::tambahBerita');
$routes->get('export_pdf/(:segment)','PdfController::index/$1');
$routes->get('export_excel','ExcelController::index');

$routes->group('admin', function($routes){
    $routes->get('/','AuthController::index',['filter' => 'auth']);
    $routes->get('logout','AuthController::logout',['filter' => 'auth']);

    $routes->get('register', 'AuthController::register');
    $routes->get('login','AuthController::login');

    $routes->post('register', 'AuthController::register_auth');
    $routes->post('login','AuthController::auth');
    
	$routes->group('category', ['filter' => 'auth'], function($routes){
        $routes->get('', 'Category::index');
        $routes->add('create','Category::create');
        $routes->add('(:segment)/edit','Category::update/$1');
        $routes->get('(:segment)/delete','Category::delete/$1');
    });
});