<?php

namespace App\Controllers;

use App\Models\ArticlesModel;
use App\Models\CategoryModel;
use CodeIgniter\Exceptions\PageNotFoundException;

class Home extends BaseController
{
    protected $articlesModels;
    protected $categoryModel;

    public function __construct()
    {
        $this->articlesModels = new ArticlesModel();
        $this->categoryModel = new CategoryModel();
    }

    public function index(): string
    {
        return view('welcome_message');
    }

    public function listContent()
    {
        #CALL DATA FROM QUERY WITH MODELS (SINGLE TABLE)
        #$data['list_data'] = $this->articlesModels->orderBy('tanggal', 'desc')->findAll();

        #CALL DATA MENGGUNAAN QUERY BUILDER ON CONTROLLER (MULTI TABLE)
        /*$data['list_data'] = $this->db->table('articles')
            ->select('articles.id, category_name, nama_status, title, content')
            ->join('categories','categories.id = articles.kategori') #INNER JOIN
            ->join('contents_status','contents_status.id = articles.draft','left')
            ->orderBy('tanggal', 'desc')->get()->getResultArray();*/
        
        #CALL DATA MENGGUNAAN QUERY BUILDER ON CONTROLLER (VIEW)
        $data['list_data'] = $this->db->table('view_berita')->orderBy('tanggal', 'desc')
        ->get()->getResultArray();
        
        $data['username'] = 'Mahasiswa 001';
        #DATA ARRAY TANPA KEY
        #$data['list_category'] = ['Data Mahasiswa', 'List Mahasiswa', 'Informasi Mahasiswa'];
        $data['list_category'] = $this->categoryModel->get_category();
        $data['list_status'] = $this->articlesModels->getStatusKonten();

        #BAGIAN UNTUK SUBMIT BERITA
        if ($this->request->getMethod() == 'post') {
            $rules = [
                'file_upload'   => 'uploaded[file_upload]|mime_in[file_upload,application/pdf,application/word]'
            ];

            $messages = [
                'file_upload' => [
                    'uploaded' => 'Anda harus melakukan unggah file pada field tersebut',
                    'mime_in' => 'File yang diunggah harus memiliki extention berupa docx,pdf'
                ]
            ];

            if ($this->validate($rules, $messages)) {
                $dataBerkas = $this->request->getFile('file_upload');
                $fileName = $dataBerkas->getRandomName();
                $data_contents = [
                    'title' => $this->request->getPost('title'),
                    'content' => $this->request->getPost('content'),
                    'file_upload' => $fileName,
                    'kategori' => $this->request->getVar('kategori'),
                    'tanggal' => date('Y-m-d H:i:s'), 
                    'draft' => $this->request->getVar('status_berita')
                ];
                $dataBerkas->move('uploads/', $fileName);
                $is_saved = $this->articlesModels->insert($data_contents);
                if ($is_saved) {
                    return redirect('content');
                }
            } else {
                $data['validation'] = $this->validator;
                return view('list_content', $data);
            }
        }

        return view('list_content', $data);
    }

    public function profile()
    {
        $data['username'] = 'Mahasiswa 001';
        $data['users'] = null;
        if ($this->request->getMethod() == 'post') {
            $data['users'] = $this->request->getPost('username');
        }

        return view('profile', $data);
    }

    public function detailContent($content_id)
    {
        $data['artikel'] = $this->articlesModels->getDetailContent($content_id);

        if (!$data['artikel']) {
            throw PageNotFoundException::forPageNotFound();
        }

        return view('detail_content', $data);
    }

    public function updateContent($content_id)
    {
        $data['artikel'] = $this->articlesModels->where('id', $content_id)->first();
        $data['list_category'] = $this->categoryModel->get_category();

        #BAGIAN UNTUK SUBMIT BERITA
        if ($this->request->getMethod() == 'post') {
            $rules = [
                'file_upload'   => 'uploaded[file_upload]|mime_in[file_upload,application/pdf,application/word]'
            ];

            $messages = [
                'file_upload' => [
                    'uploaded' => 'Harus Ada File yang diupload',
                    'mime_in' => 'File extention harus berupa docx,pdf'
                ]
            ];
            if ($this->validate($rules, $messages)) {
                $details = $this->articlesModels->find($content_id);
                if($details['file_upload']){
                    unlink('uploads/' . $details['file_upload']);
                }

                $dataBerkas = $this->request->getFile('file_upload');
                $fileName = $dataBerkas->getRandomName();
                $data_contents = [
                    'title' => $this->request->getPost('title'),
                    'content' => $this->request->getPost('content'),
                    'file_upload' => $fileName,
                    'kategori' => $this->request->getVar('kategori')
                ];
                $is_saved = $this->articlesModels->update($content_id, $data_contents);
                $dataBerkas->move('uploads/', $fileName);
                if ($is_saved) {
                    return redirect('content');
                }
            } else {
                $data['validation'] = $this->validator;
                return view('edit_content', $data);
            }
        }

        return view('edit_content', $data);
    }

    public function downloadFile($content_id)
    {
        $data = $this->articlesModels->find($content_id);
        return $this->response->download('uploads/' . $data['file_upload'], null);
    }

    public function deleteContent($content_id)
    {
        $data = $this->articlesModels->find($content_id);
        if($data['file_upload']){
            unlink('uploads/' . $data['file_upload']);
        }
        $this->articlesModels->where('id', $content_id)->delete();

        return redirect('content');
    }

    public function tambahBerita()
    {
        $rules = [
            'file_upload'   => 'uploaded[file_upload]|mime_in[file_upload,application/pdf,application/word]'
        ];

        $messages = [
            'file_upload' => [
                'uploaded' => 'Harus Ada File yang diupload',
                'mime_in' => 'File extention harus berupa docx,pdf'
            ]
        ];

        if ($this->validate($rules, $messages)) {
            $dataBerkas = $this->request->getFile('file_upload');
            $fileName = $dataBerkas->getRandomName();
            $data_contents = [
                'title' => $this->request->getPost('title'),
                'content' => $this->request->getPost('content'),
                'file_upload' => $fileName,
                'kategori' => $this->request->getVar('kategori'),
                'tanggal' => date('Y-m-d H:i:s'), 'draft' => $this->request->getVar('status_berita')
            ];
            $dataBerkas->move('uploads/', $fileName);
            $is_saved = $this->articlesModels->insert($data_contents);
            if ($is_saved) {
                return $this->response->setJSON([
                    'error' => false,
                    'message' => 'Successfully added new post!'
                ]);
            }
        } else {
            return $this->response->setJSON([
                'error' => true,
                'message' => $this->validator->getErrors()
            ]);
        }
    }

}
