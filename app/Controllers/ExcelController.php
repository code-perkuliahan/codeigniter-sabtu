<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ArticlesModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExcelController extends BaseController
{
    protected $articlesModel;

    public function __construct()
    {
        $this->articlesModel = new ArticlesModel();
    }

    public function index()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->setActiveSheetIndex(0);

        #STYLING CELL VALUE
        $header_column = [
            'alignment' => ['horizontal' => Alignment::HORIZONTAL_CENTER ],
            //'borders' => ['allBorders' => ['borderStyle' => Border::BORDER_DASHED]]
            'borders' => [
                'top' => ['borderStyle' => Border::BORDER_DASHED],
                'bottom' => ['borderStyle' => Border::BORDER_THIN],
                'right' => ['borderStyle' => Border::BORDER_DOTTED],
                'left' => ['borderStyle' => Border::BORDER_DOTTED],
            ]
        ];

        $sheet->setCellValue('A1','LIST OF CONTENTS');
        $sheet->mergeCells('A1:D1');
        $sheet->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A1:D1')->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
        $sheet->getStyle('A1:D1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FF0000');
        $sheet->getStyle('A1:D1')->getFont()->getColor()->setARGB(Color::COLOR_WHITE);

        $sheet->setCellValue('A2', 'Judul')->setCellValue('B2','Tanggal')
            ->setCellValue('C2','Kategori')
            ->setCellValue('D2','Status Berita');
        
        $sheet->getStyle('A2')->applyFromArray($header_column);
        $sheet->getStyle('B2')->applyFromArray($header_column);
        $sheet->getStyle('C2')->applyFromArray($header_column);
        $sheet->getStyle('D2')->applyFromArray($header_column);

        $rows = 3;
        $content = $this->articlesModel->getListContent();
        foreach($content as $key)
        {
            $sheet->setCellValue('A'.$rows, $key->title)
                ->setCellValue('B'.$rows, $key->tanggal)
                ->setCellValue('C'.$rows, $key->category_name)
                ->setCellValue('D'.$rows, $key->nama_status);
            $rows++;
        }

        $sheet->getColumnDimension('A')->setWidth(26);
        $sheet->getColumnDimension('B')->setWidth(20);
        $sheet->getColumnDimension('C')->setWidth(20);
        $sheet->getColumnDimension('D')->setWidth(10);

        $writer = new Xlsx($spreadsheet);
        $filename = date('Y-m-d_His').'-pwlsabtu';

        header('Content-type:application/vnd.ms-excel');
        header('Content-Disposition:attachment; filename='.$filename.'.xlsx');
        header('Cache-Control:max-age=0');

        $writer->save('php://output');
    }
}
