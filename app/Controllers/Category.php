<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\CategoryModel;

class Category extends BaseController
{
    protected $categoryModel;

    public function __construct()
    {
        $this->categoryModel = new CategoryModel();
    }

    public function index()
    {
        $data['lists'] = $this->categoryModel->orderBy('category_name','desc')->findAll();

        return view('admin/list_kategori', $data);
    }

    public function create()
    {
        #BAGIAN UNTUK SUBMIT KATEGORI
        if ($this->request->getMethod() == 'post') {
            $data_kategori = [
                'category_code' => $this->request->getPost('kode'),
                'category_name' => $this->request->getPost('nama'),
                'created_by' => $this->session->full_name, 'created_at' => date('Y-m-d H:i:s')
            ];
            $is_saved = $this->categoryModel->insert($data_kategori);
            if ($is_saved) {
                return redirect('admin/category');
            }
        }

        return view('admin/add_kategori');
    }

    public function detail()
    {
        return view('admin/detail_kategori');
    }    

    public function update($category_id)
    {
        $session = session();
        #GET DATA DETAIL CATEGORY
        $data['detail'] = $this->categoryModel->where('id', $category_id)->first();
        #BAGIAN UNTUK UPDATE KATEGORI
        if ($this->request->getMethod() == 'post') {
            $data_kategori = [
                'category_code' => $this->request->getPost('kode'),
                'category_name' => $this->request->getPost('nama'),
                'updated_by' => $session->full_name, 'updated_at' => date('Y-m-d H:i:s')
            ];
            $is_saved = $this->categoryModel->update($category_id, $data_kategori);
            if ($is_saved) {
                return redirect('admin/category');
            }
        }

        return view('admin/edit_kategori', $data);
    }

    public function delete($category_id)
    {
        #JIKA HARD DELETE, DATA YANG DIHAPUS AKAN HILANG
        $this->categoryModel->delete($category_id);
        #JIKA SOFT DELETE, DATA YANG DIHAPUS AKAN DI UPDATE STATUS
        /* $data_contents = [
            'deleted_at' => date('Y-m-d H:i:s')
        ];
        $this->categoryModels->update($category_id, $data_contents); */
        
        return redirect('admin/category');
    }
}
