<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ArticlesModel;
use Dompdf\Dompdf;

class PdfController extends BaseController
{
    protected $articlesModel;

    public function __construct()
    {
        $this->articlesModel = new ArticlesModel();
    }

    public function index($content_id)
    {
        $filename = date('Y-m-d-His').'-pwlsabtu';
        $artikel = $this->articlesModel->getDetailContent($content_id);

        #PROCESS EXPORT DATA
        $dompdf = new Dompdf();
        $dompdf->getOptions()->setChroot(FCPATH.'adminlte\img');
        $dompdf->loadHtml(view('export_pdf',['berita' => $artikel]));
        $dompdf->setPaper('A4','portrait');
        $dompdf->render();

        $dompdf->stream($filename);
    }
}
