<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ArticlesModel;

class ReportController extends BaseController
{
    protected $articlesModel;

    public function __construct()
    {
        $this->articlesModel = new ArticlesModel();
    }

    public function index()
    {
        $dataPoints = $barsPoints = $periode = [];
        $totalContent = $this->articlesModel->countAllResults();
        $contents = $this->articlesModel->getTotalKategori();
        
        //UNTUK GET DATA COLUMN-CHART
        $listStatus = $this->articlesModel->getListStatus();
        $getPeriode = $this->articlesModel->getPeriode();

        //UNTUK GET DATA PIE-CHART
        foreach ($contents as $key) {
            $dataPoints[] = [
                "name" => $key->category_name,
                "y" => floatval($key->total)
            ];
        }

        //UNTUK GET DATA COLUMN-CHART (MENGGUNAKAN NESTED ARRAY)
        foreach ($getPeriode as $key) {
            $periode[] = $key->bulan;
        }
        foreach ($listStatus as $v) {
            unset($barchart);
            $barchart = array();
            foreach ($getPeriode as $key) {
                $barcharts = $this->articlesModel->getTotalStatus($key->bulan, $v->id);
                if (isset($barcharts)) {
                    $barchart[] = intval($barcharts->total);
                }else{
                    $barchart[] = 0;
                }
            }
            $barsPoints[] = [
                "name" => $v->nama_status,
                "data" => $barchart
            ];
        }

        return view('dashboard_chart', [
            'data' => json_encode($dataPoints),
            'bars' => json_encode($barsPoints),
            'bulan' => json_encode($periode),
            'totalContent' => $totalContent
        ]);
    }
}
