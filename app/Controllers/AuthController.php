<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ArticlesModel;
use App\Models\UserModel;

class AuthController extends BaseController
{
    protected $userModel;
    protected $articleModel;

    public function __construct()
    {
        $this->userModel = new UserModel();
        $this->articleModel = new ArticlesModel();
    }

    public function index()
    {
        $years = date('Y');
        $session = session();

        $dataPoints = [];
        $total_1 = $this->articleModel->where("date_format(tanggal,'%Y')", $years)
            ->where('draft',1)->countAllResults();
        $total_2 = $this->articleModel->getMostPeriodeArticles();
        $contents = $this->articleModel->getTotalCategoryArticlesActive();
        foreach($contents as $keys)
        {
            $dataPoints[] = [
                "name" => $keys->category_name,
                "y" => floatval($keys->total)
            ];
        }

        //UNTUK GET DATA COLUMN-CHART
        $listStatus = $this->articleModel->getListStatus();
        $getPeriode = $this->articleModel->getPeriode();
        foreach ($getPeriode as $key) {
            $periode[] = $key->bulan;
        }
        foreach ($listStatus as $v) {
            unset($barchart);
            $barchart = array();
            foreach ($getPeriode as $key) {
                $barcharts = $this->articleModel->getTotalStatus($key->bulan, $v->id);
                if (isset($barcharts)) {
                    $barchart[] = intval($barcharts->total);
                }else{
                    $barchart[] = 0;
                }
            }
            $barsPoints[] = [
                "name" => $v->nama_status,
                "data" => $barchart
            ];
        }

        //UNTUK AMBIL DATA LATEST ARTICLES
        $latest_data = $this->db->table('view_berita')->orderBy('tanggal', 'desc')
        ->limit(5)->get()->getResultArray();

        return view('admin/dashboard', ['session' => $session,
            'total_1' => $total_1, 'years' => $years,
            'total_2' => $total_2,'piecharts' => json_encode($dataPoints),
            'bars' => json_encode($barsPoints), 'bulan' => json_encode($periode),
            'latest_data' => $latest_data
        ]);
    }

    # [START] LOGIN SCOPE
    public function login()
    {
        return view('auth/login');
    }
 
    public function auth()
    {
        $session = session();
        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');
        $data = $this->userModel->where('email', $email)->first();
        if($data){
            if($data->active){
                $pass = $data->password;
                $verify_pass = password_verify($password, $pass);
                if($verify_pass){
                    $ses_data = [
                        'user_id'       => $data->id,
                        'full_name'     => $data->name,
                        'user_name'     => $data->username,
                        'user_email'    => $data->email,
                        'logged_in'     => true,
                    ];
                    $session->set($ses_data);
                    return redirect()->to('admin');
                }else{
                    $session->setFlashdata('msg', 'Wrong Password');
                    return redirect('admin/login');
                }
            }else{
                $session->setFlashdata('msg', 'Data already in-active, due to user resign');
                return redirect('admin/login');
            }
        }else{
            $session->setFlashdata('msg', 'Email not Found');
            return redirect('admin/login');
        }
    }
    # [END] LOGIN SCOPE

    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('admin/login');
    }

    # [START] REGISTER SCOPE
    public function register()
    {
        $data = [];
        return view('auth/register', $data);
    }

    public function register_auth()
    {
        $rules = [
            'name'          => 'required|min_length[3]|max_length[100]',
            'email'         => 'required|min_length[6]|max_length[200]|valid_email|is_unique[users.email]',
            'password'      => 'required|min_length[6]|max_length[200]',
            'confpassword'  => 'matches[password]'
        ];
         
        if($this->validate($rules)){
            $username = explode('@', $this->request->getVar('email'));
            $data = [
                'name'     => $this->request->getPost('name'),
                'username' => strtolower($username[0]),
                'email'    => $this->request->getPost('email'),
                'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
                'active'   => true
            ];
            $this->userModel->save($data);
            return redirect()->to('admin/login');
        }else{
            $data['validation'] = $this->validator;
            return view('auth/register', $data);
        }
    }

    # [END] REGISTER SCOPE
}
