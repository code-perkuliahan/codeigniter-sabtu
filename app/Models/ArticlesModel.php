<?php

namespace App\Models;

use CodeIgniter\Model;

class ArticlesModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'articles';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $allowedFields    = ['title', 'content','tanggal','draft','file_upload','kategori'];

    public function getDetailContent($content_id)
    {
        $detail = $this->db->table('articles')
        ->select('articles.id, title, content, draft, category_name, nama_status, tanggal')
        ->join('categories','categories.id = articles.kategori') #INNER JOIN
        ->join('contents_status','contents_status.id = articles.draft','left')
        ->where('articles.id', $content_id)->get()->getResult();

        #$detail = $this->db->table('view_contents')->where()->get()->getResult();
        /*
        create views : 
        - hak akses views --> jika dump restore ke engine database yang lain
        */
        return $detail;
    }

    public function getDetailBerita($content_id)
    {
        $detail = $this->db->query("select * from articles inner join categories on categories.id = articles.kategori
            inner join contents_status on contents_status.id = articles.draft where articles.id = ".$content_id);

        $result = $detail->getRow();

        return $result;
    }

    public function getStatusKonten()
    {
        $result = [];
        $query = $this->db->table('contents_status')->get();
        $result = array('' => 'Pilih salah satu');
        foreach($query->getResult() as $key)
        {
            $result[$key->id] = $key->nama_status;
        }
        return $result;
    }

    public function getListContent()
    {
        $detail = $this->db->table('articles')
        ->select('articles.id, title, content, draft, category_name, nama_status, tanggal')
        ->join('categories','categories.id = articles.kategori') #INNER JOIN
        ->join('contents_status','contents_status.id = articles.draft','left')
        ->get()->getResult();

        return $detail;
    }

    public function getTotalKategori()
    {
        $detail = $this->db->table('articles')
        ->select('category_name, count(articles.id) as total')
        ->join('categories','categories.id = articles.kategori') #INNER JOIN
        ->groupBy('category_name')->get()->getResult();

        return $detail;
    }

    //UNTUK GET LIST STATUS
    public function getListStatus()
    {
        $result = $this->db->table('contents_status')->get()->getResult();
        return $result;
    }

    //UNTUK GET PERIODE BERDASARKAN DATA YANG ADA DI CONTENTS
    public function getPeriode()
    {
        $detail = $this->db->table('articles')->select("date_format(tanggal,'%M') as bulan")
        ->groupBy("date_format(tanggal,'%M')")->get()->getResult();

        return $detail;
    }

    //UNTUK GET TOTAL DATA ARTIKEL BERDASARKAN DATA YANG ADA DI CONTENTS
    public function getTotalStatus($bulan, $status)
    {
        $detail = $this->db->table('articles')
        ->select("count(articles.id) as total")
        ->where("date_format(tanggal,'%M')", $bulan)->where('draft',$status)
        ->groupBy("draft, date_format(tanggal,'%M')")
        ->get()->getFirstRow();

        return $detail;
    }

    public function getTotalCategoryArticlesActive()
    {
        $years = date('Y');
        $detail = $this->db->table('articles')
        ->select('category_name, count(articles.id) as total')
        ->join('categories','categories.id = articles.kategori') #INNER JOIN
        ->where("date_format(tanggal,'%Y')", $years)->where('draft',1)
        ->groupBy('category_name')->get()->getResult();

        return $detail;
    }

    public function getMostPeriodeArticles()
    {
        $years = date('Y');
        $detail = $this->db->table('articles')
        ->select("date_format(tanggal,'%M') as bulan, count(articles.id) as total")
        ->where("date_format(tanggal,'%Y')", $years)->where('draft',1)
        ->groupBy("date_format(tanggal,'%M')")->orderBy('count(id)', 'desc')
        ->get()->getFirstRow();

        return $detail;
    }
}
