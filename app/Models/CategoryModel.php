<?php

namespace App\Models;

use CodeIgniter\Model;

class CategoryModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'categories';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $allowedFields    = ['category_code','category_name','created_at','created_by','updated_at','updated_by'];

    public function get_category()
    {
        $result = [];
        $query = $this->db->table('categories')->get();
        $result = array('' => 'Pilih salah satu');
        foreach($query->getResult() as $key)
        {
            $result[$key->id] = $key->category_name;
        }
        return $result;
    }
}
