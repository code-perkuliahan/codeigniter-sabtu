<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portal CI</title> <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('css/bootstrap.min.css') ?>" />
    <style>
        .badge {
            border: 1px solid #000;
            position: relative;
            top: -1px;
            display: inline-block;
            padding: 0.25em 0.4em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: 0.25rem;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        .badge-primary {
            color: #fff;
            background-color: #007bff;
        }

        .badge-secondary {
            color: #fff;
            background-color: #6c757d;
        }

        .badge-success {
            color: #fff;
            background-color: #28a745;
        }

        .badge-info {
            color: #fff;
            background-color: #17a2b8;
        }

        .badge-warning {
            color: #212529;
            background-color: #ffc107;
        }

        .badge-danger {
            color: #fff;
            background-color: #dc3545;
        }
    </style>
</head>

<body>
    <img src="<?= FCPATH.'adminlte\img\logo.png' ?>" alt="Logo" style="width: 50px;">
    <div class="container">
        <?php foreach ($berita as $dt) : ?>
            <div class="row">
                <div class="col-md-12 my-2 card">
                    <div class="card-body">
                        <h3 class="h3"><?= $dt->title ?></h3>
                        <?php if ($dt->draft == 1) : ?>
                            <span class="badge badge-success"><?= $dt->nama_status ?></span>
                        <?php elseif ($dt->draft == 2) : ?>
                            <span class="badge badge-warning"><?= $dt->nama_status ?></span>
                        <?php elseif ($dt->draft == 3) : ?>
                            <span class="badge badge-danger"><?= $dt->nama_status ?></span>
                        <?php else : ?>
                            <span class="badge badge-secondary"><?= $dt->nama_status ?></span>
                        <?php endif; ?>
                        <h5 class="h5"><?= $dt->category_name ?></h5>
                        <p><?= $dt->content ?></p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</body>

</html>