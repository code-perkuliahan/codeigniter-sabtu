<?= $this->extend('layouts/page_layout') ?>
<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form action="" class="form" method="post" enctype="multipart/form-data">
                <?= csrf_field(); ?>
                <input type="hidden" name="id" value="<?= $artikel['id'] ?>">
                <div class="form-group"> <label for="email">Title</label> <input type="text" name="title" class="form-control" maxlength="100" value="<?= $artikel['title'] ?>"> </div>
                <div class="form-group"> <label for="email">Content</label> <textarea name="content" class="form-control" id="" cols="30" rows="10"><?= $artikel['content'] ?></textarea> </div>
                <div class="form-group"> <label for="email">Category</label><?= form_dropdown('kategori', $list_category, $artikel['kategori'], ['class' => 'form-control']) ?> </div>
                <div class="form-group">
                    <label for="berkas" class="form-label">File Attachment</label>
                    <input type="file" class="form-control" id="file_upload" name="file_upload">
                </div>

                <div class="form-group"> <input type="submit" value="Update Contents" class="btn btn-primary w-100"> </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>