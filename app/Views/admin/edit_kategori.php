<?= $this->extend('layouts/admin_layout') ?>
<?= $this->section('content') ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-tools">
                        Edit Category
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" class="form">
                    <div class="form-group">
                            <label>Category Code</label>
                            <input type="text" name="kode" class="form-control" maxlength="10" value="<?= $detail['category_code'] ?>">
                        </div>
                        <div class="form-group">
                            <label>Category Name</label>
                            <input type="text" name="nama" class="form-control" maxlength="100" value="<?= $detail['category_name'] ?>">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Update" class="btn btn-primary w-100">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>