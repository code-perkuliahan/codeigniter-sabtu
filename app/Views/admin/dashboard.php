<?= $this->extend('layouts/admin_layout') ?>
<?= $this->section('content') ?>
<h2>Welcome, <?= $session->full_name ?></h2>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-6">
                <div class="small-box bg-info">
                    <div class="inner">
                        <h4><?= $total_1 ?></h4>
                        <p>Total Active Articles <br>( Current Year )</p>
                    </div>
                    <a href="<?= base_url('admin/category') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-6">
                <div class="small-box bg-success">
                    <div class="inner">
                        <h4><?= $total_2->bulan ?> ( <?= $total_2->total ?> )</h4>
                        <p>Most Articles in Periode <br>( Current Year )</p>
                    </div>
                    <a href="<?= base_url('admin/category') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
            <section class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <div id="pie-chart"></div>
                    </div>
                </div>
            </section>
            <section class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <div id="column-chart"></div>
                    </div>
                </div>
            </section>
        </div>
        <div class="row">
            <section class="col-lg-6">
                <div class="card">
                <div class="card-header border-transparent">
                    <h3 class="card-title">Latest Articles</h3>
                    <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                    </button>
                    </div>
                </div>
                    <div class="card-body">
                    <div class="table-responsive">
                        <table class="table m-0">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Status</th>
                                <th>Date Published</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($latest_data as $list) : ?>
                                <tr>
                                    <td><?= $list['title'] ?></td>
                                    <td><?= $list['category_name'] ?></td>
                                    <td><?= $list['nama_status'] ?></td>
                                    <td><?= $list['tanggal'] ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?= $this->endSection() ?>
<?= $this->section('lib-js') ?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
    $(function() {
        Highcharts.chart('pie-chart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: "Total Active Category Periode <?= $years ?>"
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'total',
                colorByPoint: true,
                data: <?= $piecharts ?>
            }]
        });
        Highcharts.chart('column-chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: "Total Article in Periode <?= $years ?> (in status)"
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key} Status Artikel</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            xAxis: {
                categories: <?= $bulan ?>,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Status Artikel'
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: <?= $bars ?>
        });
    });
</script>
<?= $this->endSection() ?>