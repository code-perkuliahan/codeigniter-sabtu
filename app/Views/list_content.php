<?= $this->extend('layouts/page_layout') ?>
<?= $this->section('content') ?>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<form action="" class="form" method="post" enctype="multipart/form-data" id="add_content" novalidate>
				<?= csrf_field(); ?>
				<div class="form-group"> <label for="email">Title</label> <input type="text" id="title" name="title" class="form-control" maxlength="100" required> </div>
				<div class="form-group"> <label for="email">Content</label> <textarea name="content" class="form-control" id="" cols="30" rows="10" required></textarea> </div>
				<div class="form-group"> <label for="email">Category</label>
					<?= form_dropdown('kategori', $list_category, '1', ['class' => 'form-control', 'required' => true, 'id' => 'kategori']) ?>
				</div>
				<div class="form-group"> <label>Status Berita</label>
					<?= form_dropdown('status_berita', $list_status, set_value('status_berita'), ['class' => 'form-control', 'required' => true, 'id' => 'status_berita']) ?>
				</div>
				<div class="form-group">
					<label for="berkas" class="form-label">Berkas</label>
					<input type="file" class="form-control" id="file_upload" name="file_upload" required>
				</div>
				<div class="form-group"> <button type="submit" id="add_post_btn" class="btn btn-primary w-100">Submit Contents</button> </div>
			</form>
		</div>
		<div class="col-md-6">
			<a href="<?= base_url('export_excel') ?>" class="btn btn-sm btn-outline-danger">Export Content to Excel</a>
			<!-- UNTUK LIST NEWS / CONTENT USING ARRAY -->
			<?php foreach ($list_data as $news) : ?>
				<div class="col-md-12 my-2 card">
					<div class="card-body">
						<h3 class="h3"><?= $news['title'] ?></h3>
						<p><?= $news['category_name'] ?>
							<br>
							<?= $news['content'] ?>
							<br>
							<?= $news['nama_status'] ?>
						</p>
						<a href="<?= base_url('content/download/' . $news['id']) ?>" class="btn btn-sm btn-outline-success">download</a>
						<a href="<?= base_url('content/delete/' . $news['id']) ?>" class="btn btn-sm btn-outline-danger">delete</a>
						<a href="<?= base_url('content/' . $news['id']) ?>" class="btn btn-sm btn-outline-primary">read more</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<?= $this->endSection() ?>

<?= $this->section('lib-js') ?>
<script>
	jQuery(function($) {
		$("#add_content").submit(function(e) {
			e.preventDefault();
			const formData = new FormData(this);
			if (!this.checkValidity()) {
				e.preventDefault();
				$(this).addClass('was-validated');
			} else {
				$("#add_post_btn").text("On Progress...");
				$("#add_post_btn").attr("disabled", true);
				$.ajax({
					url: '<?= base_url('content/add') ?>',
					method: 'post',
					data: formData,
					contentType: false,
					cache: false,
					processData: false,
					dataType: 'json',
					success: function(response) {
						if (response.error) {
							$("#file_upload").addClass('is-invalid');
							$("#file_upload").next().text(response.message.file_upload);
							Swal.fire(
								'Error',
								response.message.file_upload,
								'error'
							)
						} else {
							$("#add_content")[0].reset();
							$("#file_upload").removeClass('is-invalid');
							$("#file_upload").next().text('');
							$("#add_content").removeClass('was-validated');
							Swal.fire(
								'Added',
								response.message,
								'success'
							).then(function() {
								window.location = "<?= base_url('content') ?>";
							});
						}
						$("#add_post_btn").text("Submit Contents");
						$("#add_post_btn").attr("disabled", false);
					}
				});
			}
		});

		$('#kategori').on('change', function(e) {
			console.log(this.value);
			if (this.value == 3) {
				$("#status_berita").val(4);
			}
		});

		$('#status_berita').on('change', function(e) {
			console.log(this.value);
			if (this.value == 3) {
				$("#add_post_btn").attr("disabled", true);
			} else {
				$("#add_post_btn").attr("disabled", false);
			}
		});

		$('#title').on('keyup', function(e) {
			var title = this.value;
			console.log(title.length);
			if (title.length >= 20) {
				Swal.fire(
					'Warning',
					'Judul tidak boleh lebih dari 20 karakter',
					'warning'
				).then(function() {
					$("#add_post_btn").attr("disabled", true);
				});
			} else {
				$("#add_post_btn").attr("disabled", false);
			}
		});
	});
</script>
<?= $this->endSection() ?>