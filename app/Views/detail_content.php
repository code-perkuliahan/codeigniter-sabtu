<?= $this->extend('layouts/page_layout') ?>
<?= $this->section('content') ?>
<div class="container">
    <?php foreach($artikel as $dt) : ?>
    <div class="row">
        <div class="col-md-12 my-2 card">
            <div class="card-body">
                <h3 class="h3"><?= $dt->title ?></h3>
                <span><?= $dt->nama_status ?></span>
                <h5 class="h5"><?= $dt->category_name ?></h5>
                <p><?= $dt->content ?></p>
                <a href="<?= base_url('content/edit/'.$dt->id) ?>" class="btn btn-sm btn-outline-primary">edit</a>
                <a href="<?= base_url('export_pdf/'.$dt->id) ?>" class="btn btn-sm btn-outline-success">pdf</a>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
<?= $this->endSection() ?>